This resourcepack is a fork of [GTNH Textures Usernm](https://github.com/Usernm0/GTNH-Textures-Usernm) for the modpack [GregTech Lite](https://gitlab.com/sweep_tosho).

Thanks for [Usernm0](https://github.com/Usernm0)'s wonderful work! The original resourcepack is on CC-BY 4.0 License, and this fork is also.